﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimVINWarranty : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfContractID.Value = Request.QueryString["contractid"];
            if (!IsPostBack)
            {
                GetVIN();
                if (hfVIN.Value.Length > 0)
                    FillPage();
            }
        }

        protected void GetVIN()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            SQL = "select VIN from contract c " +
                "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfVIN.Value = clC.GetFields("vin");
            }
        }

        protected void FillPage()
        {
            string sql;
            clsDBO clV = new clsDBO();

            sql = "select * from vin.dbo.vin v " +
                "inner join vin.dbo.warranty bd on bd.vinid = v.vinid " +
                "where vin = '" + hfVIN.Value.Substring(0, 11) + "' ";
            rgWarranty.DataSource = clV.GetData(sql, ConfigurationManager.AppSettings["vinstring"]);
        }

    }
}