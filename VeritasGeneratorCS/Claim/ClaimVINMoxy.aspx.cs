﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;


namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimVINMoxy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfContractID.Value = Request.QueryString["contractid"];
            if (!IsPostBack)
                FillVehicleInfo();
        }

        protected void FillVehicleInfo()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            clsDBO clCS = new clsDBO();
            SQL = "select c.contractid, vin, class, year,make,model,trim,awd,turbo,diesel,hybrid,commercial,liftkit,snowplow from contract c " +
                "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                lblVIN.Text = clC.GetFields("vin");
                lblClass.Text = clC.GetFields("class");
                lblMake.Text = clC.GetFields("make");
                lblYear.Text = clC.GetFields("year");
                lblModel.Text = clC.GetFields("model");
                lblTrim.Text = clC.GetFields("trim");
                SQL = "select * from contractsurcharge " +
                    "where contractid = " + clC.GetFields("contractid");
                clCS.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clCS.RowCount() > 0)
                {
                    clCS.GetRow();
                    chkAWD.Checked = Convert.ToBoolean(clCS.GetFields("AWD"));
                    chkTurbo.Checked = Convert.ToBoolean(clCS.GetFields("Turbo"));
                    chkDiesel.Checked = Convert.ToBoolean(clCS.GetFields("Diesel"));
                    chkHybrid.Checked = Convert.ToBoolean(clCS.GetFields("Hybrid"));
                    chkCommercial.Checked = Convert.ToBoolean(clCS.GetFields("Commercial"));
                    chkHydraulic.Checked = Convert.ToBoolean(clCS.GetFields("Hydraulic"));
                    chkAirBladder.Checked = Convert.ToBoolean(clCS.GetFields("AirBladder"));
                    chkLiftKit.Checked = Convert.ToBoolean(clCS.GetFields("LiftKit"));
                    chkLuxury.Checked = Convert.ToBoolean(clCS.GetFields("Luxury"));
                    chkSeals.Checked = Convert.ToBoolean(clCS.GetFields("Seals"));
                    chkSnowPlow.Checked = Convert.ToBoolean(clCS.GetFields("SnowPlow"));
                    chkLargerLiftKit.Checked = Convert.ToBoolean(clCS.GetFields("LargerLiftKit"));
                    chkRideShare.Checked = Convert.ToBoolean(clCS.GetFields("rideshare"));
                    chkLuxuryNC.Checked = Convert.ToBoolean(clCS.GetFields("LuxuryNC"));
                    chkChrome.Checked = Convert.ToBoolean(clCS.GetFields("chrome"));
                    chkCosmetic.Checked = Convert.ToBoolean(clCS.GetFields("cosmetic"));
                }
            }
        }
    }
}