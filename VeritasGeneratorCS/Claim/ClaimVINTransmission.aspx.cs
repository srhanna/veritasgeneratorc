﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimVINTransmission : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfContractID.Value = Request.QueryString["Contractid"];
            if (!IsPostBack)
            {
                GetVIN();
                if (hfVIN.Value.Length > 0)
                    FillPage();
            }
        }

        private void GetVIN()
        {
            clsDBO clC = new clsDBO();
            string SQL = "select vin from contract c where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfVIN.Value = clC.GetFields("vin");
            }
        }

        private void FillPage()
        {
            clsDBO clV = new clsDBO();
            string SQL = "select * from vin.dbo.vin v ";
            SQL = SQL + "inner join vin.dbo.transmission bd on bd.vinid = v.vinid ";
            SQL = SQL + "where vin = '" + hfVIN.Value.Substring(0, 11) + "' ";
            clV.OpenDB(SQL, ConfigurationManager.AppSettings["vinstring"]);
            if (clV.RowCount() > 0)
            {
                clV.GetRow();
                txtAvailability.Text = clV.GetFields("availability");
                txtDetail.Text = clV.GetFields("detailtype");
                txtGears.Text = clV.GetFields("gears");
                txtName.Text = clV.GetFields("name");
                txtOrderCode.Text = clV.GetFields("ordercode");
                txtTransmissionType.Text = clV.GetFields("transmissiontype");
                chkFleet.Checked = Convert.ToBoolean(clV.GetFields("fleet"));
            }
        }
    }
}