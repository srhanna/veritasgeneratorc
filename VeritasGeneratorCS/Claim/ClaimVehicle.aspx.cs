﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Configuration;
using System.Diagnostics;


namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimVehicle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
    
            hfContractID.Value = Request.QueryString["ContractID"];
            pvMoxy.ContentUrl = "ClaimVINMoxy.aspx?ContractID=" + hfContractID.Value;
            pvBasicData.ContentUrl = "ClaimVINBasicData.aspx?ContractID=" + hfContractID.Value;
            pvEngine.ContentUrl = "ClaimVINEngine.aspx?ContractID=" + hfContractID.Value;
            pvTransmission.ContentUrl = "ClaimVINTransmission.aspx?ContractID=" + hfContractID.Value;
            pvWarranty.ContentUrl = "ClaimVINWarranty.aspx?ContractID=" + hfContractID.Value;

            if (!IsPostBack)
            {
                pvMoxy.Selected = true;
                tsClaim.SelectedIndex = 0;
                FillVehicleInfo();
            }

        }

        protected void FillVehicleInfo()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            VeritasGlobalToolsV2.clsVIN clV = new VeritasGlobalToolsV2.clsVIN();
            if (hfContractID.Value.Length == 0)
                return;
            SQL = "select * from contract " +
                "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                clV.Contract = clC.GetFields("contractno");
                clV.VIN = clC.GetFields("vin");
                clV.ProcessVIN();
                hfVIN.Value = clC.GetFields("vin");
            }
        }

        protected void tsClaim_TabClick(object sender, RadTabStripEventArgs e)
        {
            if (tsClaim.SelectedTab.Value == "Moxy")
                pvMoxy.Selected = true;
            if (tsClaim.SelectedTab.Value == "Basic")
                pvBasicData.Selected = true;
            if (tsClaim.SelectedTab.Value == "Engine")
                pvEngine.Selected = true;
            if (tsClaim.SelectedTab.Value == "Transmission")
                pvTransmission.Selected = true;
            if (tsClaim.SelectedTab.Value == "Warranty")
                pvWarranty.Selected = true;
        }
    }
}