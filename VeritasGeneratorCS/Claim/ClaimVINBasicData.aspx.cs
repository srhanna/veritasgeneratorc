﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimVINBasicData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfContractID.Value = Request.QueryString["ContractID"];
            if (!IsPostBack)
            {
                GetVIN();
                if (hfVIN.Value.Length > 0)
                {
                    FillPage();
                }
            }
        }

        protected void GetVIN()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            SQL = "select VIN from contract c " +
                "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfVIN.Value = clC.GetFields("vin");
            }
        }

        protected void FillPage()
        {
            string SQL;
            clsDBO clV = new clsDBO();
            SQL = "select * from vin.dbo.vin v " +
                "inner join vin.dbo.basicdata bd on bd.vinid = v.vinid " +
                "where vin = '" + hfVIN.Value.Substring(0, 11) + "' ";
            clV.OpenDB(SQL, ConfigurationManager.AppSettings["vinstring"]);
            if (clV.RowCount() > 0)
            {
                clV.GetRow();
                txtYear.Text = clV.GetFields("year");
                txtMake.Text = clV.GetFields("make");
                txtModel.Text = clV.GetFields("model");
                txtTrim.Text = clV.GetFields("trim");
                txtVehicleType.Text = clV.GetFields("vehicletype");
                txtBodyType.Text = clV.GetFields("bodytype");
                txtBodySubType.Text = clV.GetFields("bodysubtype");
                txtOEMBodyStyle.Text = clV.GetFields("oembodystyle");
                txtDoors.Text = clV.GetFields("door");
                txtOEMDoors.Text = clV.GetFields("oemdoor");
                txtModelNumber.Text = clV.GetFields("modelnumber");
                txtPackageCode.Text = clV.GetFields("packagecode");
                txtPackageSummary.Text = clV.GetFields("packagesummary");
                txtRearAxle.Text = clV.GetFields("rearaxle");
                txtDriveType.Text = clV.GetFields("drivetype");
                txtBrakeSystem.Text = clV.GetFields("BrakeSystem");
                txtRestraintType.Text = clV.GetFields("RestraintType");
                txtCountryMfr.Text = clV.GetFields("countrymfr");
                txtPlant.Text = clV.GetFields("plant");
                txtChassisType.Text = clV.GetFields("chassistype");
                chkGlider.Checked = Convert.ToBoolean(clV.GetFields("glider"));
            }

        }
    }
}