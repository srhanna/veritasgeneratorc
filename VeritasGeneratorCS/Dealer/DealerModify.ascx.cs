﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Dealer
{
    public partial class DealerModify : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsDealerStatus.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsAgent.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsSubAgent.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();

            if (!IsPostBack)
            {
                pnlDetail.Visible = true;
                pnlSearchAgent.Visible = false;
                pnlSearchSubAgent.Visible = false;
                hfDealerID.Value = Request.QueryString["dealerid"];
                FillDealer();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillDealer()
        {
            string SQL;
            clsDBO clD = new clsDBO();
            SQL = "select * from dealer where dealerid = " + hfDealerID.Value;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                txtDealerNo.Text = clD.GetFields("dealerno");
                txtDealerName.Text = clD.GetFields("dealername");
                txtDealerEMail.Text = clD.GetFields("email");
                txtPhone.Text = clD.GetFields("phone");
                cboDealerStatus.SelectedValue = clD.GetFields("dealerstatusid");
                hfAgentID.Value = clD.GetFields("agentsid");
                txtAgentName.Text = Functions.GetAgentInfo(long.Parse(hfAgentID.Value));
                hfSubAgentID.Value = clD.GetFields("subagentid");
                txtAddr1.Text = clD.GetFields("addr1");
                txtAddr2.Text = clD.GetFields("addr2");
                txtCity.Text = clD.GetFields("city");
                cboState.SelectedValue = clD.GetFields("state");
                txtZip.Text = clD.GetFields("zip");
                txtMoxyEntry.Text = clD.GetFields("moxyentry");
                txtDateSigned.Text = clD.GetFields("datesigned");
                lblCreBy.Text = Functions.GetUserInfo(long.Parse(clD.GetFields("creby")));
                lblCreDate.Text = clD.GetFields("credate");
                lblModBy.Text = clD.GetFields("modby");
                lblModDate.Text = clD.GetFields("moddate");
                txtDBA.Text = clD.GetFields("dba");
                txtEIN.Text = clD.GetFields("ein");
            }
            else
            {
                txtDealerNo.Text = "";
                txtDealerEMail.Text = "";
                txtDealerName.Text = "";
                txtPhone.Text = "";
                cboDealerStatus.SelectedValue = 1.ToString();
                hfAgentID.Value = 0.ToString();
                txtAgentName.Text = "";
                hfSubAgentID.Value = 0.ToString();
                txtAddr1.Text = "";
                txtAddr2.Text = "";
                txtCity.Text = "";
                cboState.SelectedValue = "";
                txtZip.Text = "";
                txtMoxyEntry.Text = "";
                txtDateSigned.Text = "";
                lblCreBy.Text = "";
                lblCreDate.Text = "";
                lblModBy.Text = "";
                lblModDate.Text = "";
                txtDBA.Text = "";
                txtEIN.Text = "";
            }
        }
        private long GetDealerID()
        {
            string SQL;
            clsDBO clD = new clsDBO();
            SQL = "select max(dealerid) as DealerID from dealer " +
                  "where creby = " + hfUserID.Value;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow(); ;
                return long.Parse(clD.GetFields("dealerid"));
            }
            return 0;
        }

        protected void btnSeekAgent_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSearchAgent.Visible = true;
        }

        protected void rgAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfAgentID.Value = rgAgent.SelectedValue.ToString();
            txtAgentName.Text = Functions.GetAgentInfo(long.Parse(rgAgent.SelectedValue.ToString()));
            pnlDetail.Visible = true;
            pnlSearchAgent.Visible = false;
        }

        protected void btnSeekSubAgent_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSearchSubAgent.Visible = true;
        }

        protected void rgSubAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfSubAgentID.Value = rgSubAgent.SelectedValue.ToString();
            pnlDetail.Visible = true;
            pnlSearchSubAgent.Visible = false;
            string SQL;
            clsDBO clA = new clsDBO();
            SQL = "select * from subagents where subagentid = " + rgSubAgent.SelectedValue;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                hfAgentID.Value = clA.GetFields("agentid");
                txtAgentName.Text = Functions.GetAgentInfo(long.Parse(clA.GetFields("agentid")));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            FillDealer();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clD = new clsDBO();
            SQL = "select * from dealer where dealerid = " + hfDealerID.Value;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
            }
            else
            {
                clD.NewRow();
            }
            clD.SetFields("dealerno", txtDealerNo.Text);
            clD.SetFields("dealername", txtDealerName.Text);
            clD.SetFields("addr1", txtAddr1.Text);
            clD.SetFields("addr2", txtAddr2.Text);
            clD.SetFields("city", txtCity.Text);
            clD.SetFields("state", cboState.SelectedValue);
            clD.SetFields("zip", txtZip.Text);
            clD.SetFields("phone", txtPhone.Text);
            clD.SetFields("agentsid", hfAgentID.Value);
            clD.SetFields("subagentid", hfSubAgentID.Value);
            if (DateTime.TryParse(txtMoxyEntry.Text, out DateTime result))
            {
                clD.SetFields("moxyentry", txtMoxyEntry.Text);
            }
            if (DateTime.TryParse(txtDateSigned.Text, out DateTime result2))
            {
                clD.SetFields("datesigned", txtDateSigned.Text);
            }
            clD.SetFields("email", txtDealerEMail.Text);
            clD.SetFields("dba", txtDBA.Text);
            clD.SetFields("ein", txtEIN.Text);
            clD.SetFields("dealerstatusid", cboDealerStatus.SelectedValue);
            if (clD.RowCount() == 0)
            {
                clD.SetFields("creby", hfUserID.Value);
                clD.SetFields("credate", DateTime.Today.ToString());
                clD.AddRow();
            }
            else
            {
                clD.SetFields("moddate", DateTime.Today.ToString());
                clD.SetFields("modby", hfUserID.Value);
            }
            clD.SaveDB();
            RedirectDealer();
        }

        private void RedirectDealer()
        {
            if (hfDealerID.Value != "0")
            {
                Response.Redirect("dealer.aspx?sid=" + hfID.Value + "&dealerid=" + hfDealerID.Value);
            }
            else
            {
                Response.Redirect("dealer.aspx?sid=" + hfID.Value + "&dealerid=" + GetDealerID());
            }
        }

        protected void btnAgentClose_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlSearchAgent.Visible = false;
        }

        protected void btnClearSubAgent_Click(object sender, EventArgs e)
        {
            hfSubAgentID.Value = 0.ToString();
            pnlDetail.Visible = true;
            pnlSearchSubAgent.Visible = false;
        }
    }
}