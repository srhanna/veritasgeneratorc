﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerModify.ascx.cs" Inherits="VeritasGeneratorCS.Dealer.DealerModify" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel ID="pnlDetail" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Dealer No:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtDealerNo" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Dealer Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtDealerName" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Dealer E-Mail:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtDealerEMail" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Phone:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Status:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboDealerStatus" DataSourceID="dsDealerStatus" DataTextField="DealerStatus" DataValueField="DealerStatusID" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsDealerStatus"
                            ProviderName="System.Data.SqlClient" SelectCommand="select DealerStatusID, DealerStatus from dealerstatus" runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Agent Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtAgentName" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnSeekAgent" OnClick="btnSeekAgent_Click" runat="server" Text="Seek" CssClass="button1" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Address 1:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtAddr1" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Address 2:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtAddr2" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            City:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtCity" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            State:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsStates"
                            ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Zip Code:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtZip" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Moxy Entry:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtMoxyEntry" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Date Signed:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtDateSigned" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            DBA:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtDBA" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            EIN:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtEIN" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Create Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblCreDate" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Create by:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblCreBy" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Modify Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblModDate" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Modify By:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblModBy" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Table runat="server" HorizontalAlign="Right">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Width="100" Text="Save" CssClass="button2" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Width="100" Text="Cancel" CssClass="button1" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="pnlSearchAgent" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <telerik:RadGrid ID="rgAgent" OnSelectedIndexChanged="rgAgent_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                    AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsAgent">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="AgentID" PageSize="10" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="AgentID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AgentNo" UniqueName="AgentNo" HeaderText="Agent No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentName" HeaderText="Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="dsAgent" ProviderName="System.Data.SqlClient" SelectCommand="select agentid, agentno, agentname, city, state from agents" runat="server"></asp:SqlDataSource>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Button ID="btnAgentClose" OnClick="btnAgentClose_Click" runat="server" Width="100" Text="Close" BackColor="#1eabe2" BorderColor="#1eabe2" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="pnlSearchSubAgent" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <telerik:RadGrid ID="rgSubAgent" OnSelectedIndexChanged="rgSubAgent_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                    AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsSubAgent">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="SubAgentID" PageSize="10" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="SubAgentID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SubAgentNo" UniqueName="SubAgentNo" HeaderText="Sub Agent No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SubAgentName" UniqueName="SubAgentName" HeaderText="Sub Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="dsSubAgent"
                ProviderName="System.Data.SqlClient" SelectCommand="select subagentid, subagentno, subagentname, city, state from subagents" runat="server"></asp:SqlDataSource>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table runat="server" Width="900">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:Button ID="btnClearSubAgent" runat="server" Width="100" Text="Clear Sub Agent" BackColor="#1eabe2" BorderColor="#1eabe2" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnSubAgentClose" runat="server" Width="100" Text="Close" BackColor="#1eabe2" BorderColor="#1eabe2" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:HiddenField ID="hfAgentID" runat="server" />
<asp:HiddenField ID="hfSubAgentID" runat="server" />
<asp:HiddenField ID="hfDealerID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />

