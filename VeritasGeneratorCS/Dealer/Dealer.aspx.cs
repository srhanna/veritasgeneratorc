﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Dealer
{
    public partial class Dealer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                Session["DealerID"] = Request.QueryString["dealerid"];
                FillDealer();
            }
            if (hfError.Value == "Visible")
            {
                rwError.VisibleOnPageLoad = true;
            }
            else
            {
                rwError.VisibleOnPageLoad = false;
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            hfDealerID.Value = Request.QueryString["dealerid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
            }
        }
        private void FillDealer()
        {
            tsDealer.Tabs[0].Selected = true;
            pvContact.Selected = true;

            string sSQL = "select * from dealer where dealerid = " + Session["DealerID"].ToString();//hfDealerID.Value;


            clsDBO dBO = new clsDBO();
            dBO.OpenDB(sSQL, ConfigurationManager.AppSettings["connstring"]);

            if (dBO.RowCount() > 0)
            {
                dBO.GetRow();
                lblDealerNo.Text = dBO.GetFields("dealerno");
                lblDealerName.Text = dBO.GetFields("dealername");
                lblAddr1.Text = dBO.GetFields("addr1");
                lblAddr2.Text = dBO.GetFields("addr2");
                lblAddr3.Text = dBO.GetFields("city") + ", " + dBO.GetFields("state") + " " + dBO.GetFields("zip");
                FillAgents(Convert.ToInt32(dBO.GetFields("agentsid")));
                FillStatus(Convert.ToInt32(dBO.GetFields("dealerstatusid")));
                lblDateSigned.Text = dBO.GetFields("datesigned");
                lblMoxyEntry.Text = dBO.GetFields("moxyentry");
                if (String.IsNullOrEmpty(dBO.GetFields("salvagetitle")))
                    chkSalvage.Checked = false;
                else
                    chkSalvage.Checked = Convert.ToBoolean(dBO.GetFields("salvagetitle"));
            }
        }

        private void FillAgents(int agentID)
        {
            clsDBO dBO = new clsDBO();
            string sSQL = "select * from agents where agentid = " + agentID;
            dBO.OpenDB(sSQL, ConfigurationManager.AppSettings["connstring"]);
            if (dBO.RowCount() > 0)
            {
                dBO.GetRow();
                lblAgentName.Text = dBO.GetFields("agentname");
                lblAgentNo.Text = dBO.GetFields("agentno");
            }
        }
        private void FillStatus(int dlrStatusID)
        {
            clsDBO dBO = new clsDBO();
            string sSQL = "select * from dealerstatus where dealerstatusid = " + dlrStatusID;
            dBO.OpenDB(sSQL, ConfigurationManager.AppSettings["connstring"]);
            if (dBO.RowCount() > 0)
            {
                dBO.GetRow();
                lblStatus.Text = dBO.GetFields("dealerstatus");
            }
        }


        protected void tsDealer_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsDealer.SelectedTab.Value == "Contact")
            {
                tsDealer.Tabs[0].Selected = true;
                pvContact.Selected = true;
            }
            if (tsDealer.SelectedTab.Value == "Note")
            {
                tsDealer.Tabs[1].Selected = true;
                pvNote.Selected = true;
            }
            if (tsDealer.SelectedTab.Value == "Contract")
            {
                tsDealer.Tabs[3].Selected = true;
                pvContract.Selected = true;
            }
            if (tsDealer.SelectedTab.Value == "Documents")
            {
                tsDealer.Tabs[2].Selected = true;
                pvDocument.Selected = true;
            }
            if (tsDealer.SelectedTab.Value == "Commission")
            {
                tsDealer.Tabs[4].Selected = true;
                pvCommission.Selected = true;
            }
            if (tsDealer.SelectedTab.Value == "Modify")
            {
                tsDealer.Tabs[5].Selected = true;
                pvModify.Selected = true;
            }
            if (tsDealer.SelectedTab.Value == "Overfund")
            {
                tsDealer.Tabs[6].Selected = true;
                pvOverfund.Selected = true;
            }
        }
        protected void chkSalvage_CheckedChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update dealer ";
            if (chkSalvage.Checked)
            {
                SQL = SQL + "set salvagetitle = " + 1 + " ";
            }
            else
            {
                SQL = SQL + "set salvagetitle = " + 0 + " ";
            }
            SQL = SQL + "where dealerid = " + hfDealerID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }
        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }
        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }
    }
}