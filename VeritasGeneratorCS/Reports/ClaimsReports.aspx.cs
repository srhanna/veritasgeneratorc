﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Reports
{
    public partial class ClaimsReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                FillReports();
            }
            if (hfError.Value == "Visible")
            {
                rwError.VisibleOnPageLoad = true;
            }
            else
            {
                rwError.VisibleOnPageLoad = false;
            }
        }
        private void FillReports()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select reportid, reportno, reportname from reports where reportcategory = 'Claims' ";
            if (CheckANOnly())
            {
                SQL = SQL + "and reportid in (1032,1034,1068) ";
            }
            if (hfVeroOnly.Value.ToLower() == "true")
            {
                SQL = SQL + "and reportid in (1042,1068) ";
            }
            if (hfAgentID.Value.Length > 0)
            {
                if (hfAgentID.Value != "0")
                {
                    SQL = SQL + "and reportid in (1031,1033,1068) ";
                }
            }
            rgReport.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgReport.Rebind();
        }
        private bool CheckANOnly()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            hfVeroOnly.Value = "false";
            bool returnVar = false;
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and autonationonly <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                returnVar = true;
            }
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and veroonly <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                hfVeroOnly.Value = "true";
            }
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfAgentID.Value = clR.GetFields("agentid");
                hfSubAgentID.Value = clR.GetFields("subagentid");
            }
            return returnVar;
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
            btnPhoneReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) 
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) 
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) 
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) 
                {
                    btnDealer.Enabled = true;
                }
                //if (Convert.ToBoolean(clSI.GetFields("phonereports")) == true) 
                //{
                //    btnPhoneReports.Enabled = true;
                //}
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) 
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) 
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) 
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) 
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) 
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) 
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnUsers_Click(object sender, EventArgs e) {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e) {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e) {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/salesreportssearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e) {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e) {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e) {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnPhoneReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/phonereports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnCustomReports_Click(object sender, EventArgs e)
        {

        }

        protected void rgReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(rgReport.SelectedValue) == 1006) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimStats.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1007) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimPendingPaymentAge.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1011) 
            {
                Response.Redirect("~/reports/claimsreportspages/ANLossCode.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1012) 
            {
                Response.Redirect("~/reports/claimsreportspages/SingleMultipleClaim.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1017) 
            {
                Response.Redirect("~/reports/claimsreportspages/PaidDenied.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1018) 
            {
                Response.Redirect("~/reports/claimsreportspages/ANClaim.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1020) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimApprovedMonth.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1021) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimApprovedWeek.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1022) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimPaidReport.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1026) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimApprovedDaily.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1027) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimsOpenAll.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1028) 
            {
                Response.Redirect("~/reports/claimsreportspages/OpenedClaims.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1029) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimAccess.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1030) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimAdjusterRank.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1031) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimsOpenLive.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1032) 
            {
                Response.Redirect("~/reports/claimsreportspages/ANClaimOpenLive.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1033) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimOpenApprovedLive.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1034) 
            {
                Response.Redirect("~/reports/claimsreportspages/ANClaimOpenApprovedLive.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1035) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimOpenLiveByTeam.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1036) 
            {
                Response.Redirect("~/reports/claimsreportspages/ANClaimOpenLiveByTeam.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1037) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimOpenApprovedLiveByTeam.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1038) 
            {
                Response.Redirect("~/reports/claimsreportspages/ANClaimOpenApprovedLiveByTeam.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1039) 
            {
                Response.Redirect("~/reports/claimsreportspages/MonthEndClaimReserveCMF.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1040) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimDataPoint.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1041) 
            {
                Response.Redirect("~/reports/claimsreportspages/ServiceCenterClaim.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1042) 
            {
                Response.Redirect("~/reports/claimsreportspages/VeroClaimOpenLive.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1043) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimAuthorized.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1044) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimsOnline.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1045) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimCMF.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1046) 
            {
                Response.Redirect("~/reports/claimsreportspages/SpecialClaimOpen.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1047) 
            {
                Response.Redirect("~/reports/claimsreportspages/OpenInspection.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1048) 
            {
                Response.Redirect("~/reports/claimsreportspages/WebClaimOpen.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1049) 
            {
                Response.Redirect("~/reports/claimsreportspages/ANClaimBalance.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1051) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimPaymentError.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1052) 
            {
                Response.Redirect("~/reports/claimsreportspages/AZClaimOpenLive.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1053) 
            {
                Response.Redirect("~/reports/claimsreportspages/ANRMF.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1054) 
            {
                Response.Redirect("~/reports/claimsreportspages/ANClaimsOpenWaitingRF.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1055) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimOpenWaitingRF.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1056) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimRMF.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1062) 
            {
                Response.Redirect("~/reports/claimsreportspages/AgentClaimCnt.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1063) 
            {
                Response.Redirect("~/reports/claimsreportspages/VerifyClaimStatusAN.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1064) 
            {
                Response.Redirect("~/reports/claimsreportspages/VerifyClaimStatus.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1068) 
            {
                Response.Redirect("~/reports/claimsreportspages/ClaimReportByDealer.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgReport.SelectedValue) == 1069) 
            {
                Response.Redirect("~/reports/claimsreportspages/SeverityDetail.aspx?sid=" + hfID.Value);
            }
        }
    }
}