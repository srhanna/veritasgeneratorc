﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Agents
{
    public partial class AgentsModify : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsAgentStatus.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            SqlDataSource1.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();
            hfAgentID.Value = Request.QueryString["AgentID"];
            pnlModify.Visible = true;
            pnlSearch.Visible = false;
            if (!IsPostBack)
            {
                FillAgent();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillAgent()
        {
            string SQL;
            clsDBO clA = new clsDBO();
            SQL = "select * from agents where agentid = " + hfAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                txtAddr1.Text = clA.GetFields("addr1");
                txtAddr2.Text = clA.GetFields("addr2");
                txtAgentName.Text = clA.GetFields("agentname");
                txtAgentNo.Text = clA.GetFields("agentno");
                txtCity.Text = clA.GetFields("city");
                txtContact.Text = clA.GetFields("contact");
                txtEIN.Text = clA.GetFields("ein");
                txtPhone.Text = clA.GetFields("Phone");
                txtEMail.Text = clA.GetFields("email");
                cboAgentStatus.SelectedValue = clA.GetFields("statusid");
                hfParentAgentID.Value = clA.GetFields("parentagentid");
                txtParentAgent.Text = GetParentAgentInfo(long.Parse(clA.GetFields("parentagentid")));
                cboState.SelectedValue = clA.GetFields("state");
                txtZip.Text = clA.GetFields("zip");
            }
            else
            {
                txtAddr1.Text = "";
                txtAddr2.Text = "";
                txtAgentName.Text = "";
                txtAgentNo.Text = "";
                txtCity.Text = "";
                txtContact.Text = "";
                txtEIN.Text = "";
                txtPhone.Text = "";
                txtEMail.Text = "";
                hfParentAgentID.Value = 0.ToString();
                txtParentAgent.Text = "";
                cboState.Text = "";
                txtZip.Text = "";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            FillAgent();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clA = new clsDBO();

            SQL = "select * from agents where agentid = " + hfAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                clA.SetFields("moddate", DateTime.Today.ToString());
                clA.SetFields("modby", hfUserID.Value);
            }
            else
            {
                clA.NewRow();
                clA.SetFields("credate", DateTime.Today.ToString());
                clA.SetFields("creby", hfUserID.Value);
            }
            clA.SetFields("addr1", txtAddr1.Text);
            clA.SetFields("addr2", txtAddr2.Text);
            clA.SetFields("city", txtCity.Text);
            clA.SetFields("agentname", txtAgentName.Text);
            clA.SetFields("agentno", txtAgentNo.Text);
            clA.SetFields("contact", txtContact.Text);
            clA.SetFields("phone", txtPhone.Text);
            clA.SetFields("ein", txtEIN.Text);
            clA.SetFields("email", txtEMail.Text);
            clA.SetFields("parentagentid", hfParentAgentID.Value);
            clA.SetFields("statusid", cboAgentStatus.SelectedValue);
            clA.SetFields("state", cboState.SelectedValue);
            clA.SetFields("zip", txtZip.Text);
            if (clA.RowCount() == 0)
            {
                clA.AddRow();
            }
            clA.SaveDB();
            RedirectAgent();
        }
        private void RedirectAgent()
        {
            if (hfAgentID.Value != "0")
            {
                Response.Redirect("agents.aspx?sid=" + hfID.Value + "&AgentID=" + hfAgentID.Value);
            }
            else
            {
                Response.Redirect("agents.aspx?sid=" + hfID.Value + "&AgentID=" + GetAgentID());
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            pnlModify.Visible = false;
            pnlSearch.Visible = true;
        }

        protected void rgAgents_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfParentAgentID.Value = rgAgents.SelectedValue.ToString();
            txtParentAgent.Text = GetParentAgentInfo(long.Parse(hfParentAgentID.Value));
            pnlModify.Visible = true;
            pnlSearch.Visible = false;
        }
        private long GetAgentID()
        {
            string SQL;
            clsDBO clA = new clsDBO();
            SQL = "select max(agentid) as agentid from agents where creby = " + hfUserID.Value;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                return long.Parse(clA.GetFields("agentid"));
            }
            return 0;
        }
        private string GetParentAgentInfo(long xParentAgentID)
        {
            if (xParentAgentID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO clA = new clsDBO();
            SQL = "select * from agents where agentid = " + xParentAgentID;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                return clA.GetFields("agentno") + " / " + clA.GetFields("agentname");
            }
            return "";
        }
    }
}