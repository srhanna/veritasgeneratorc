﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS
{
    static public class Functions
    {
        static public string GetUserInfo(long xUserID)
        {
            if (xUserID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO clUI = new clsDBO();
            SQL = "select * from userinfo where userid = " + xUserID;
            clUI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clUI.RowCount() > 0)
            {
                clUI.GetRow();
                return clUI.GetFields("fname") + " " + clUI.GetFields("lname");
            }
            return "";
        }
        static public string GetUserEmail(long xUserID)
        {
            if (xUserID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO clUI = new clsDBO();
            SQL = "select * from userinfo where userid = " + xUserID;
            clUI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clUI.RowCount() > 0)
            {
                clUI.GetRow();
                return clUI.GetFields("email");
            }
            return "";
        }
        static public string GetSubAgentInfo(long xID)
        {
            if (xID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO clSA = new clsDBO();
            SQL = "select * from subagents where subagentid = " + xID;
            clSA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSA.RowCount() > 0)
            {
                clSA.GetRow();
                return clSA.GetFields("subagentno") + " / " + clSA.GetFields("subagentname");
            }
            return "";
        }
        static public string GetAgentInfo(long xID)
        {
            if (xID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO clA = new clsDBO();
            SQL = "select * from agents where agentid = " + xID;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                return clA.GetFields("agentno") + " / " + clA.GetFields("agentname");
            }
            return "";
        }
        static public string GetRateType(long xID)
        {
            if (xID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO clRT = new clsDBO();
            SQL = "select * from ratetype " + "where ratetypeid = " + xID;
            clRT.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clRT.RowCount() > 0)
            {
                clRT.GetRow();
                return clRT.GetFields("ratetypename");
            }
            return "";
        }
        static public bool GetAllow45(long xUserID)
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + xUserID + " " +
                  "and allow45days <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        static public string GetServiceCenterState(long xClaimID)
        {
            //GetServiceCenterState = ""
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select sc.State from claim c " +
                  "inner join ServiceCenter sc on c.ServiceCenterID = sc.ServiceCenterID " +
                  "where claimid = " + xClaimID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("state");
            }
            return "";
        }

        public static void SetTestColor(Panel pnlHeader, Image image)
        {
            if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                image.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                image.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
            }
        }

        public static string GetLienholder(string xContractNo)
        {
            string SQL = "select * from contract where contractNo = '" + xContractNo + "' ";
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                return clC.GetFields("lienholder");
            }

            return "";
        }

        public static string GetContractID(string xContractNo)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from contract ";
            SQL = SQL + "where contractNo = '" + xContractNo + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("contractid");
            }

            return "0";
        }

        public static string GetUserName(string hfIDValue)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            string sGetUserName = "";
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfIDValue;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sGetUserName = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
            return sGetUserName;
        }

        public static string GetFromName(string hfIDValue)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            string sGetFromName = "";
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfIDValue;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sGetFromName = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
            return sGetFromName;
        }

        public static string GetPayee(long xID)
        {
            if (xID == 0)
                return "";

            clsDBO clP = new clsDBO();
            string SQL = "select * from payee where payeeid = " + xID;
            clP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                return clP.GetFields("CompanyName");
            }

            return "";
        }

        public static long DateDiffHours(DateTime d1, DateTime d2)
        {
            TimeSpan datediff = (d2.Subtract(d1));
            return Convert.ToInt64(datediff.TotalHours);
        }

        public static long DateDiffDays(DateTime d1, DateTime d2)
        {
            TimeSpan datediff = (d2.Subtract(d1));
            return datediff.Days;
        }

        public static string Right(string original, int numberCharacters)
        {
            return original.Substring(original.Length - numberCharacters, numberCharacters);

            //return Strings.Right(original, numberCharacters);
        }

        public static string GetContractFirstName(string xContractNo)
        {
            clsDBO clR = new clsDBO();
            string SQL = "select fname from contract where contractno = '" + xContractNo + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("fname");
            }

            return "";
        }

        public static string GetContractLastName(string xContractNo)
        {
            clsDBO clR = new clsDBO();
            string SQL = "select lname from contract where contractno = '" + xContractNo + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("lname");
            }

            return "";
        }
    }
}