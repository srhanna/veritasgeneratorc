﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.VisualBasic;

namespace VeritasGeneratorCS.Contract
{
    public partial class ContractModification : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsLienholderSelect.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsTermMonthSelect.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsPlanTypeSelect.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsTermMileSelect.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsTermMonthSelect.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            LienholderSelect.Items.Insert(0, "None");


            if (!IsPostBack)
            {
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                ReadOnlyButtons();
                FillHistory();
                GetPlanType(hfContractID.Value);

                GetLienholder();
                PlanTypeSelect.SelectedText = hfPlanType.Value;
                TermMonthSelect.SelectedText = hfTermMonth.Value;
                TermMileSelect.SelectedText = hfTermMiles.Value;
                
            }
        }


        private void GetLienholder()
        {
            string SQL = "select * from contract where contractid = '" + hfContractID.Value + "' ";
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                LienholderSelect.SelectedValue = clC.GetFields("lienholder");
            }
        }

        private void GetPlanType(string contractid)
        {
            string SQL = "select p.plantypeid, p.plantype from contract c" +
                " inner join plantype p on p.plantypeid = c.plantypeid where c.contractid = " + contractid;
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfPlanType.Value = clC.GetFields("plantype");
                hfPlanTypeID.Value = clC.GetFields("plantypeid");
            }

            GetTermMonthandMiles();
        }

        private void GetTermMonthandMiles()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfTermMonth.Value = clR.GetFields("TermMonth");
                hfTermMiles.Value = clR.GetFields("TermMile");
            }
        }



        private void FillHistory()
        {
            clsDBO clH = new clsDBO();
            string SQL;
            SQL = "select ContractHistoryID, ContractID, fieldname, oldvalue, newvalue, credate, username from contracthistory ch " +
                  "inner join UserInfo ui on ui.userid = ch.CreBy " +
                  "where contractid = " + hfContractID.Value + " " +
                  "order by credate desc ";
            rgHistory.DataSource = clH.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (!Convert.ToBoolean(clR.GetFields("contractmodification")))
                {
                    btnUpdateSaleDate.Enabled = false;
                    btnUpdateSaleMile.Enabled = false;
                    //btnSave.Visible = False;
                }
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnUpdateSaleMile_Click(object sender, EventArgs e)
        {
            clsDBO clC = new clsDBO();
            string SQL;
            long lMileDiff;
            if (txtNewSaleMile.Text.Length == 0)
            {
                return;
            }
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                lMileDiff = long.Parse(clC.GetFields("effmile")) - long.Parse(clC.GetFields("salemile"));
                UpdateHistory("SaleMile", clC.GetFields("salemile"), txtNewSaleMile.Text);
                clC.SetFields("salemile", txtNewSaleMile.Text);
                clC.SetFields("effmile", (long.Parse(txtNewSaleMile.Text) + lMileDiff).ToString());
                clC.SetFields("expmile", (long.Parse(clC.GetFields("effmile")) + long.Parse(clC.GetFields("termmile"))).ToString());
                clC.SaveDB();
                Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
            }
        }

        protected void btnUpdateSaleDate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clC = new clsDBO();
            long lDayDiff;
            if (txtNewSaleDate.Text.Length == 0)
            {
                return;
            }
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                //lDayDiff = DateDiff(DateInterval.Day, CDate(clC.Fields("saledate")), CDate(clC.Fields("effdate")))
                lDayDiff = (DateTime.Parse(clC.GetFields("effdate")) - DateTime.Parse(clC.GetFields("saledate"))).Days;
                UpdateHistory("SaleDate", clC.GetFields("saledate"), txtNewSaleDate.Text);
                clC.SetFields("saledate", txtNewSaleDate.Text);
                clC.SetFields("effdate", DateTime.Parse(clC.GetFields("saledate")).AddDays(lDayDiff).ToString()); //DateAdd(DateInterval.Day, lDayDiff, CDate(clC.Fields("saledate")))) 
                clC.SetFields("expdate", DateTime.Parse(clC.GetFields("effdate")).AddMonths(Convert.ToInt32(clC.GetFields("termmonth"))).ToString()); //DateAdd(DateInterval.Month, CLng(clC.Fields("termmonth")), CDate(clC.Fields("effdate")))
                clC.SaveDB();
                Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
            }
        }
        private void UpdateHistory(string xFieldName, string xOldValue, string xNewValue)
        {
            string SQL;
            clsDBO clC = new clsDBO();
            SQL = "select * from contracthistory where contracthistoryid =  0 ";
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clC.NewRow();
            clC.SetFields("contractid", hfContractID.Value);
            clC.SetFields("fieldname", xFieldName);
            clC.SetFields("OldValue", xOldValue);
            clC.SetFields("newvalue", xNewValue);
            clC.SetFields("creby", hfUserID.Value);
            clC.SetFields("credate", DateTime.Today.ToString());
            clC.AddRow();
            clC.SaveDB();
        }

        protected void rgHistory_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                FillHistory();
            }
        }

        protected void TermMileSelect_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            hfTermMiles.Value = TermMileSelect.SelectedValue;
        }

        protected void TermMonthSelect_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            hfTermMiles.Value = TermMonthSelect.SelectedValue;
        }

        protected void PlanTypeSelect_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            hfPlanType.Value = PlanTypeSelect.SelectedText;
            hfPlanTypeID.Value = PlanTypeSelect.SelectedValue;
        }

        protected void btnUpdatePlan_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(txtDealerCost.Text) || !Information.IsNumeric(txtDealerCost.Text))
            {
                ShowError();
                return;
            }

            clsDBO clR = new clsDBO();
            string SQL;

            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToDouble(txtDealerCost.Text) < (Convert.ToDouble(clR.GetFields("moxydealercost")) - 250)
                    || Convert.ToDouble(txtDealerCost.Text) == Convert.ToDouble(clR.GetFields("moxydealercost")))
                {
                    ShowError();
                    return;
                }

                UpdateHistory("moxydealercost", clR.GetFields("moxydealercost"), txtDealerCost.Text);
                clR.SetFields("moxydealercost", txtDealerCost.Text);
                UpdateHistory("plantypeid", clR.GetFields("plantypeid"), hfPlanTypeID.Value);
                clR.SetFields("plantypeid", hfPlanTypeID.Value);
                if (txtCustomerCost.Text.Length > 0)
                {
                    UpdateHistory("customercost", clR.GetFields("customercost"), txtCustomerCost.Text);
                    clR.SetFields("customercost", txtCustomerCost.Text);
                }

                UpdateHistory("termmonth", clR.GetFields("termmonth"), hfTermMonth.Value);
                UpdateHistory("termmile", clR.GetFields("termmile"), hfTermMiles.Value);

                if (clR.GetFields("termmonth") != hfTermMonth.Value)
                    clR.SetFields("expdate", Convert.ToDateTime(clR.GetFields("effdate")).AddMonths(Convert.ToInt32(hfTermMonth.Value)).ToString());

                clR.SetFields("termmonth", hfTermMonth.Value);

                if (clR.GetFields("termmile") != hfTermMiles.Value)
                    clR.SetFields("expmile", (Convert.ToInt64(clR.GetFields("effmile")) + Convert.ToInt64(hfTermMiles.Value)).ToString());

                clR.SetFields("termmile", hfTermMiles.Value);

                clR.SaveDB();
                Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);

            }
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void LienholderSelect_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            hfLienholder.Value = LienholderSelect.SelectedValue;
        }

        protected void btnUpdateLienholder_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from contract c where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(LienholderSelect.SelectedValue) || LienholderSelect.SelectedValue == "None")
                {
                    clsDBO clT = new clsDBO();
                    string SQL2 = "update contract set lienholder = NULL where contractid = " + hfContractID.Value;
                    clT.RunSQL(SQL2, ConfigurationManager.AppSettings["connstring"]);

                }
                else
                    clR.SetFields("lienholder", LienholderSelect.SelectedValue);

                clR.SaveDB();
                Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
            }
        }
    }
}