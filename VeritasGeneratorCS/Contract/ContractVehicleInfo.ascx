﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractVehicleInfo.ascx.cs" Inherits="VeritasGeneratorCS.Contract.ContractVehicleInfo" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server" Font-Names="Calibri" Font-Size="11">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlViewer" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            VIN:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblVIN" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Class:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblClass" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Year:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblYear" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Make:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblMake" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Model:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblModel" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Trim:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblTrim" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkAWD" Text="AWD" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkTurbo" Text="Turbo" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkDiesel" Text="Diesel" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkHybrid" Text="Hybrid" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkCommercial" Text="Commercial" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkHydraulic" Text="Hydraulic" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkAirBladder" Text="Air Bladder" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkLiftKit" Text="Lift Kit" Enabled="false" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkLuxury" Text="Luxury" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkSeals" Text="Seals" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkSnowPlow" Text="Snow Plow" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkLargerLiftKit" Text="Larger Lift Kit" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkRideShare" Text="Rideshare" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkSalvageTitle" Text="Salvage Title Authorized" Enabled="false" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" CssClass="button1" Text="Edit" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlModify" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            VIN:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtVINMod" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Class:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtClassMod" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Year:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtYearMod" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Make:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtMakeMod" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Model:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtModelMod" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Trim:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtTrimMod" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkAwdMod" Text="AWD" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkTurboMod" Text="Turbo" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkDieselMod" Text="Diesel" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkHybridMod" Text="Hybrid"  runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkCommercialMod" Text="Commercial"  runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkHydraulicMod" Text="Hydraulic"  runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkAirBladderMod" Text="Air Bladder"  runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkLiftKitMod" Text="Lift Kit" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkLuxuryMod" Text="Luxury" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkSealsMod" Text="Seals"  runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkSnowPlowMod" Text="Snow Plow"  runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkLargerLiftKitMod" Text="Larger Lift Kit"  runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkRideShareMod" Text="Rideshare"  runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnCancel" OnClick="btnCancel_Click" CssClass="button1" runat="server" Text="Cancel" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="button2" runat="server" Text="Save" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>

<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfContractID" runat="server" />
