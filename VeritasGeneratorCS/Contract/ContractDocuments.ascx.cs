﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Contract
{
    public partial class ContractDocuments : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            hfContractID.Value = Request.QueryString["contractid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlList.Visible = true;
                pnlAdd.Visible = false;
                pnlDetail.Visible = false;
                FillGrid();
                ReadOnlyButtons();
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly").ToLower() == "true")
                {
                    btnSave.Enabled = false;
                    btnUpload.Enabled = false;
                    btnDelete.Enabled = false;
                }
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillGrid()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select contractdocumentid, documentname, documentdesc, documentlink " +
                  "from contractdocument " +
                  "where contractid = " + hfContractID.Value + " " +
                  "and deleted = 0 ";
            rgContractDocument.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgContractDocument.Rebind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlAdd.Visible = true;
            pnlList.Visible = false;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string sDocLink;
            string SQL;
            clsDBO clR = new clsDBO();
            GetContractNo();
            string folderPath = Server.MapPath("~") + "\\documents\\contracts\\" + hfContractNo.Value + "_" + FileUpload2.FileName;
            sDocLink = "~/documents/contracts/" + hfContractNo.Value + "_" + FileUpload2.FileName;
            FileUpload2.SaveAs(folderPath);
            SQL = "select * from contractdocument where contractid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.NewRow();
            clR.SetFields("contractid", hfContractID.Value);
            clR.SetFields("documentname", txtDocName.Text);
            clR.SetFields("documentdesc", txtDocDesc.Text);
            clR.SetFields("documentlink", sDocLink);
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("credate", DateTime.Now.ToString());
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.AddRow();
            clR.SaveDB();
            pnlAdd.Visible = false;
            pnlList.Visible = true;
            FillGrid();
        }
        private void GetContractNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select contractno from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractNo.Value = clR.GetFields("contractno");
            }
        }
        protected void rgContractDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            hfDocID.Value = rgContractDocument.SelectedValue.ToString();
            FillDetail();
        }
        private void FillDetail()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from contractdocument where contractdocumentid = " + hfDocID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtCreBy.Text = Functions.GetUserInfo(long.Parse(clR.GetFields("creby")));
                txtCreDate.Text = clR.GetFields("credate");
                txtDescDetail.Text = clR.GetFields("documentdesc");
                txtModBy.Text = Functions.GetUserInfo(long.Parse(clR.GetFields("modby")));
                txtTitleDetail.Text = clR.GetFields("documentname");
                txtModDate.Text = clR.GetFields("moddate");
            }
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update contractdocument " +
                  "set deleted = 1 " +
                  "where contractdocumentid = " + hfDocID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * contractdocument where contractdocumentid = " + hfDocID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("documentname", txtTitleDetail.Text);
                clR.SetFields("documentdesc", txtDescDetail.Text);
                clR.SetFields("modby", hfUserID.Value);
                clR.SetFields("moddate", DateTime.Now.ToString());
                clR.SaveDB();
            }
        }

        protected void btnCloseAdd_Click(object sender, EventArgs e)
        {
            pnlAdd.Visible = false;
            pnlList.Visible = true;
        }
    }
}