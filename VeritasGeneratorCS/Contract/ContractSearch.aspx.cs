﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Contract
{
    public partial class ContractSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
            }
            
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfInsCarrierID.Value = clSI.GetFields("inscarrierid");
                btnUsers.Enabled = true;
                hfVeroOnly.Value = Convert.ToBoolean(clSI.GetFields("veroonly")).ToString();
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) {
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }
        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SearchContracts();
        }
        private void SearchContracts()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            SQL = "select contractid, contractno, dealername, fname, lname, c.city, c.state, VIN, make, model, saledate, a.agentname " +
                  "from contract c " +
                  "left join dealer d on c.dealerid = d.dealerid " +
                  "left join agents a on c.agentsid = a.agentid " +
                  "where contractid > 0 ";

            if (txtContractNo.Text.Length > 0) {
                SQL = SQL + "and c.contractno like '%" + txtContractNo.Text.Trim() + "%' ";
            }
            if (txtDealerNo.Text.Length > 0) {
                SQL = SQL + "and d.dealerno like '%" + txtDealerNo.Text.Trim() + "%' ";
            }
            if (txtDealername.Text.Length > 0) {
                SQL = SQL + "and d.dealername like '%" + txtDealername.Text.Trim() + "%' ";
            }
            if (txtAgentName.Text.Length > 0) {
                SQL = SQL + "and a.agentname like '%" + txtAgentName.Text.Trim() + "%' ";
            }
            if (txtFName.Text.Length > 0) {
                SQL = SQL + "and c.fname like '%" + txtFName.Text.Trim() + "%' ";
            }
            if (txtLName.Text.Length > 0) {
                SQL = SQL + "and c.lname like '%" + txtLName.Text.Trim() + "%' ";
            }
            if (txtCity.Text.Length > 0) {
                SQL = SQL + "and c.city like '%" + txtCity.Text.Trim() + "%' ";
            }
            if (cboState.Text.Length > 0) {
                SQL = SQL + "and c.state like '%" + cboState.Text.Trim() + "%' ";
            }
            if (txtPhone.Text.Length > 0) {
                SQL = SQL + "and c.phone like '%" + txtPhone.Text.Trim() + "%' ";
            }
            if (DateTime.TryParse(txtSaleDate.Text, out DateTime result)) {
                SQL = SQL + "and saledate = '" + txtSaleDate.Text.Trim() + "' ";
            }
            if (DateTime.TryParse(txtPayDate.Text, out DateTime result2)) {
                SQL = SQL + "and datepaid = '" + txtPayDate.Text.Trim() + "' ";
            }
            if (txtVIN.Text.Length > 0) {
                SQL = SQL + "and vin like '%" + txtVIN.Text.Trim() + "%' ";
            }
            if (txtCheckNo.Text.Length > 0) {
                SQL = SQL + "and contractid in (select contractid from contractpayment where checkno like '%" + txtCheckNo.Text.Trim() + "%' ";
            }
            if (Convert.ToInt32(hfInsCarrierID.Value) > 0) {
                SQL = SQL + "and inscarrierid = " + hfInsCarrierID.Value + " ";
            }
            if (hfVeroOnly.Value == "True") {
                SQL = SQL + "and a.agentid = 54 ";
            }
            SQL = SQL + "order by lname ";
            rgContractID.DataSource = clC.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgContractID.DataBind();
        }

        protected void rgContractID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/contract.aspx?sid=" + hfID.Value + "&contractid=" + rgContractID.SelectedValue);
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }
        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }
        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void rgContractID_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            SearchContracts();
        }

        protected void rgContractID_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort" || e.CommandName == "ChangePageSize")
            {
                SearchContracts();
            }
        }
    }
}