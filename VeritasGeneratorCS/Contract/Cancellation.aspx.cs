﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Contract
{
    public partial class Cancellation : System.Web.UI.Page
    {
        private long lContractID;
        private bool bOOB;
        private bool bRegulatedState;
        protected void Page_Load(object sender, EventArgs e)
        {
            string relativePath = "~/api/export/file";
            RadClientExportManager1.PdfSettings.Fonts.Add("monospace", "");
            //the font is added to export the code viewer properly
            RadClientExportManager1.SvgSettings.ProxyURL = ResolveUrl(relativePath);
            lContractID = long.Parse(Request.QueryString["contractID"]);
            FillContractCancellation();
        }
        private void FillContractCancellation()
        {
            string SQL;
            clsDBO clCC = new clsDBO();
            clsDBO clC = new clsDBO();
            clsDBO clD = new clsDBO();
            clsDBO clP = new clsDBO();
            long lDaysElapse;
            SQL = "select * from contract where contractid = " + lContractID;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                SQL = "select * from contractcancel where contractid = " + lContractID;
                clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clCC.RowCount() > 0)
                {
                    clCC.GetRow();
                    lblContractNo.Text = clC.GetFields("contractno");
                    lblVIN.Text = clC.GetFields("vin");
                    if (clC.GetFields("contractno").Substring(0, 3) == "CHJ")
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "DRV")
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "RAC")
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "RAD")
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "RAN")
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "RDI") 
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "REP") 
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "RSA") 
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "RSD") 
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "RSW") 
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "SAG") 
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "VEL") 
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }
                    else if (clC.GetFields("Contractno").Substring(0, 3) == "VIS") 
                    {
                        Image1.ImageUrl = "~/images/RSAdmin.png";
                        pnlHeader.BackColor = System.Drawing.Color.White;
                    }


                    lblCustomer.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                    lblSaleDate.Text = DateTime.Parse(clC.GetFields("saledate")).ToString("M/d/yyyy");
                    if (clCC.GetFields("canceleffdate").Length > 0)
                    {
                        lblEffDate.Text = DateTime.Parse(clCC.GetFields("canceleffdate")).ToString("M/d/yyyy");
                    }
                    lblEffMile.Text = long.Parse(clC.GetFields("effmile")).ToString("#,###");
                    lblCancelMile.Text = long.Parse(clCC.GetFields("cancelmile")).ToString("#,###");
                    lblTotalDays.Text = DateTime.Parse(clC.GetFields("expdate")).ToString("M/d/yyyy");
                    lblTotalMiles.Text = clC.GetFields("expmile");
                    lblDealerCostBase.Text = Convert.ToDouble(clC.GetFields("moxydealercost")).ToString("#,##0.00");
                    lblCustomerCost.Text = Convert.ToDouble(clC.GetFields("customercost")).ToString("#,##0.00");
                    //lDaysElapse = DateDiff(DateInterval.Day, CDate(Format(CDate(clC.GetFields("saledate")), "M/d/yyyy")), CDate(clCC.GetFields("canceleffdate")))
                    lDaysElapse = (DateTime.Parse(clCC.GetFields("canceleffdate")) - DateTime.Parse(DateTime.Parse(clC.GetFields("saledate")).ToString("M/d/yyyy"))).Days;
                    lblDaysElapse.Text = lDaysElapse.ToString("#,##0");
                    lblMilesElapse.Text = (long.Parse(clCC.GetFields("cancelmile")) - long.Parse(clC.GetFields("effmile"))).ToString("#,##0");
                    lblCancelFee.Text = Convert.ToDouble(clCC.GetFields("cancelfeeamt")).ToString("c");
                    lblClaims.Text = Convert.ToDouble(clCC.GetFields("claimamt")).ToString("c");
                    GetDealerInfo(long.Parse(clC.GetFields("dealerid")));
                    lblLienholder.Text = clC.GetFields("lienholder");
                    if (clCC.GetFields("payeeid").Length > 0)
                    {
                        GetPayee(long.Parse(clCC.GetFields("payeeid")));
                    }
                    lblMilePer.Text = Convert.ToDouble(clCC.GetFields("milefactor")).ToString("0.###%");
                    lblTimePer.Text = Convert.ToDouble(clCC.GetFields("termfactor")).ToString("0.###%");
                    lblRefundPer.Text = Convert.ToDouble(clCC.GetFields("cancelfactor")).ToString("0.###%");
                    lblDealerCost.Text = Convert.ToDouble(clCC.GetFields("dealeramt")).ToString("c");
                    CheckRegulatedState(clC.GetFields("state"));
                    double dCustRefund;
                    if (bOOB)
                    {
                        if (bRegulatedState)
                        {
                            if (Convert.ToDouble(clCC.GetFields("dealeramt")) > Convert.ToDouble(clCC.GetFields("adminamt")))
                            {
                                lblAdminCost.Text = Convert.ToDouble(clCC.GetFields("adminamt")).ToString("currency");
                                dCustRefund = Convert.ToDouble(clCC.GetFields("dealeramt")) + Convert.ToDouble(clCC.GetFields("adminamt"));
                            }
                            else
                            {
                                lblAdminCost.Text = (Convert.ToDouble(clCC.GetFields("adminamt")) - Convert.ToDouble(clCC.GetFields("dealeramt"))).ToString("currency");
                                dCustRefund = Convert.ToDouble(clCC.GetFields("adminamt"));
                            }
                        }
                        else
                        {
                            dCustRefund = Convert.ToDouble(clCC.GetFields("dealeramt")) + Convert.ToDouble(clCC.GetFields("adminamt"));
                            lblAdminCost.Text = Convert.ToDouble(clCC.GetFields("adminamt")).ToString("currency");
                        }
                    }
                    else
                    {
                        dCustRefund = Convert.ToDouble(clCC.GetFields("dealeramt")) + Convert.ToDouble(clCC.GetFields("adminamt"));
                        lblAdminCost.Text = Convert.ToDouble(clCC.GetFields("adminamt")).ToString("currency");
                    }
                    if (bRegulatedState)
                    {
                        lblRegulated.Text = "Yes";
                    }
                    else
                    {
                        lblRegulated.Text = "No";
                    }
                    lblCustomerRefund.Text = dCustRefund.ToString("c");
                    lblCancelInfo.Text = clCC.GetFields("cancelinfo");
                    if (clC.GetFields("status") == "Cancelled")
                    {
                        rCancel.Visible = true;
                        rCancel2.Visible = true;
                        rCancel3.Visible = true;
                        rQuote.Visible = false;
                    }
                    else
                    {
                        rCancel.Visible = false;
                        rCancel.Visible = false;
                        rCancel.Visible = false;
                        rQuote.Visible = true;
                    }
                }
            }
        }
        private void CheckRegulatedState(string xState)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from cancelstaterules where state = '" + xState + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("regulated")))
                {
                    bRegulatedState = true;
                }
            }
        }
        private void GetPayee(long xPayeeID)
        {
            clsDBO clP = new clsDBO();
            string SQL;
            SQL = "select * from payee where payeeid = " + xPayeeID;
            clP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                if (clP.GetFields("companyname").Length > 0)
                {
                    lblPayee.Text = clP.GetFields("companyname");
                }
                else
                {
                    lblPayee.Text = clP.GetFields("fname") + " " + clP.GetFields("lname");
                }
                lblAddr1.Text = clP.GetFields("addr1");
                if (clP.GetFields("addr2").Length > 0)
                {
                    lblAddr2.Text = clP.GetFields("addr2");
                    lblAddr3.Text = clP.GetFields("city") + " " + clP.GetFields("state") + " " + clP.GetFields("zip");
                }
                else
                {
                    lblAddr2.Text = clP.GetFields("city") + " " + clP.GetFields("state") + " " + clP.GetFields("zip");
                }
            }
        }
        private void GetDealerInfo(long xDealerID)
        {
            string SQL;
            clsDBO clD = new clsDBO();
            SQL = "select * from dealer where dealerid = " + xDealerID;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                lblDealer.Text = clD.GetFields("dealername");
                if (Convert.ToInt32(clD.GetFields("dealerstatusid")) == 5)
                {
                    bOOB = true;
                }
            }
        }
    }
}